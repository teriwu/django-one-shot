from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from todos.models import TodoList, TodoItem


# Create your views here.
class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todo_lv"
    paginate_by = 4


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    

class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("list_todos")


class ItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/icreate.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.id])


class ItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/iupdate.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.id])
